#include <bits/stdc++.h>

#define ll long long
#define pb push_back
#define pii pair<int, int>
#define pll pair<ll, ll>
#define el endl
#define mi map<int, int>
#define mll map<ll, ll>
#define umi unordered_map<int, int>
#define umll unordered_map<ll, ll>
#define vi vector<int>
#define vll vector<ll>
#define all(vect) vect.begin(), vect.end()
#define reset(A) memset(A, 0, sizeof(A))
#define approx(n) fixed << setprecision(n)

using namespace std;

const int mod = 1e9 + 7;

void solve()
{
  int n, time;
  cin >> n >> time;
  int res = 0;
  mi mp;
  for(int i = 0; i < n; i++)
  {
    int a, b;
    cin >> a >> b;
    mp[b] = max(mp[b], a);
  }
  for(int i = 0; i < time; i++)
  {
    res += mp[i];
  }
  cout << res << el;
}

int main()
{
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);

  int t = 1;
  // cin >> t;
  // cin.ignore();
  while (t--)
  {
    solve();
  }
  return 0;
}

/*
4 4
1000 1
500 2
1200 0
3000 2
*/