#include<bits/stdc++.h>

using namespace std;

int main() {
    int t;
    cin >> t;
    while(t--) {
        int n;
        cin >> n;
        int a[n];
        int minn = INT_MAX;
        int res = 0;
        vector<int> v;
        for(int i = 0; i < n; i++) {
            cin >> a[i];
            minn = min(minn, a[i]);
        }
        for(int i = 0; i < n; i++) {
            res += int(a[i] /(minn * 2));
        }
        cout << res << endl;
    }
}