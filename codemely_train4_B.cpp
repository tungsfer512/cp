#include <bits/stdc++.h>

#define ll long long
#define pb push_back
#define pii pair<int, int>
#define pll pair<ll, ll>
#define el endl
#define mi map<int, int>
#define mll map<ll, ll>
#define umi unordered_map<int, int>
#define umll unordered_map<ll, ll>
#define vi vector<int>
#define vll vector<ll>
#define all(vect) vect.begin(), vect.end()
#define reset(A) memset(A, 0, sizeof(A))
#define approx(n) fixed << setprecision(n)

using namespace std;

const int mod = 1e9 + 7;

void solve()
{
    int m, n;
    cin >> m >> n;
    string a[m];
    for (int i = 0; i < m; i++)
    {
        cin >> a[i];
    }
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (a[i][j] == 'S')
            {
                if (i == 0)
                {
                    if (j == 0)
                    {
                        if (a[i + 1][j] == 'W' || a[i][j + 1] == 'W')
                        {
                            cout << "No";
                            return;
                        }
                    }
                    else if (j == n - 1)
                    {
                        if (a[i + 1][j] == 'W' || a[i][j - 1] == 'W')
                        {
                            cout << "No";
                            return;
                        }
                    }
                    else
                    {
                        if (a[i + 1][j] == 'W' || a[i][j + 1] == 'W' || a[i][j - 1] == 'W')
                        {
                            cout << "No";
                            return;
                        }
                    }
                }
                else if (j == 0)
                {
                    if (i == m - 1)
                    {
                        if (a[i - 1][j] == 'W' || a[i][j + 1] == 'W')
                        {
                            cout << "No";
                            return;
                        }
                    }
                    else
                    {
                        if (a[i + 1][j] == 'W' || a[i][j + 1] == 'W' || a[i - 1][j] == 'W')
                        {
                            cout << "No";
                            return;
                        }
                    }
                }
                else if (i == m - 1)
                {
                    if (j == n - 1)
                    {
                        if (a[i - 1][j] == 'W' || a[i][j - 1] == 'W')
                        {
                            cout << "No";
                            return;
                        }
                    }
                    else
                    {
                        if (a[i - 1][j] == 'W' || a[i][j + 1] == 'W' || a[i][j - 1] == 'W')
                        {
                            cout << "No";
                            return;
                        }
                    }
                }
                else if (j == n - 1)
                {
                    if (a[i + 1][j] == 'W' || a[i - 1][j] == 'W' || a[i][j - 1] == 'W')
                    {
                        cout << "No";
                        return;
                    }
                }
                else
                {
                    if (a[i - 1][j] == 'W' || a[i + 1][j] == 'W' || a[i][j - 1] == 'W' || a[i][j + 1] == 'W')
                    {
                        cout << "No";
                        return;
                    }
                }
            }
        }
    }
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (a[i][j] == '.')
            {
                a[i][j] = 'D';
            }
        }
    }
    cout << "Yes" << el;
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            cout << a[i][j];
        }
        cout << el;
    }
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int t = 1;
    // cin >> t;
    // cin.ignore();
    while (t--)
    {
        solve();
    }
    return 0;
}

/*

*/