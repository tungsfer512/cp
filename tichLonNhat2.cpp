#include <bits/stdc++.h>

#define ll long long
#define pb push_back
#define pii pair<int, int>
#define pll pair<ll, ll>
#define el endl
#define umi unordered_map<ll, ll>
#define umll unordered_map<ll, ll>
#define vi vector<int>
#define vll vector<ll>
#define all(vect) vect.begin(), vect.end()
#define reset(A) memset(A, 0, sizeof(A))
#define approx(n) fixed << setprecision(n)

using namespace std;

const int mod = 1e9 + 7;

void solve()
{
  int n;
  cin >> n;
  vi v;
  vi res;
  int te;
  for (int i = 0; i < n; i++)
  {
    cin >> te;
    v.pb(te);
    if (i >= 2)
    {
      int min1 = INT_MAX, min2 = INT_MAX;
      int max1 = INT_MIN, max2 = INT_MIN, max3 = INT_MIN;
      for (int n : v)
      {
        if (n <= min1)
        {
          min2 = min1;
          min1 = n;
        }
        else if (n <= min2)
        {
          min2 = n;
        }
        if (n >= max1)
        {
          max3 = max2;
          max2 = max1;
          max1 = n;
        }
        else if (n >= max2)
        {
          max3 = max2;
          max2 = n;
        }
        else if (n >= max3)
        {
          max3 = n;
        }
      }
      res.pb(max(min1 * min2 * max1, max1 * max2 * max3));
    }
    else
    {
      res.pb(0);
    }
  }
  for (int i = 0; i < res.size(); i++)
  {
    cout << res[i] << " ";
  }
  cout << el;
}

int main()
{
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);

  int t = 1;
  // cin >> t;
  // cin.ignore();
  while (t--)
  {
    solve();
  }
  return 0;
}

/*
5
10 -10 20 -2 3
*/