#include<bits/stdc++.h>

using namespace std;

#define ull unsigned long long
const ull MAX =  1e19 + 7;

ull mul(ull a, ull b) {
    return (a <= MAX / b)? a * b : MAX;
}

int  main() {
    vector<ull> v;
    map<ull, ull> mp;
    ull te1 = 1, te2 = 1, te3 = 1;
    for(ull i = 0; i < 60; i++) {
        te2 = 1;
        for(ull j = 0; j < 38; j++) {
            te3 = 1;
            for(ull k = 0; k < 26; k++) {
                ull x = mul(te1, mul(te2, te3));
                if(x < MAX && x > 0) {
                    v.push_back(x);
                }
                te3 = mul(te3, 5);
            }
            te2 = mul(te2, 3);
        }
        te1 = mul(te1, 2);
    }
    sort(v.begin(), v.end());

    for(ull i = 0; i < v.size(); i++) {
        mp[v[i]] = i + 1;
    }

    ull t;
    cin >> t;

    while(t--) {
        ull n;
        cin >> n;
        if(mp[n] != 0) {
            cout << mp[n] << endl;
        }
        else {
            cout << "Not in sequence" << endl;
        }
    }
}

/*
11
1
2
6
7
8
9
10
11
12
13
14
*/