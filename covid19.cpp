#include <bits/stdc++.h>

#define ll long long
#define pb push_back
#define pii pair<int, int>
#define pll pair<ll, ll>
#define el endl
#define umi unordered_map<int, int>
#define umll unordered_map<ll, ll>
#define vi vector<int>
#define vll vector<ll>
#define all(vect) vect.begin(), vect.end()
#define reset(A) memset(A, 0, sizeof(A))
#define approx(n) fixed << setprecision(n)

using namespace std;

const int mod = 1e9 + 7;

void solve()
{
  while (true)
  {
    int n;
    cin >> n;
    int cnt = 0;
    if (n == 0)
    {
      return;
    }
    int a[n];
    for (int i = 0; i < n; i++)
    {
      cin >> a[i];
    }
    for (int i = 1; i < n - 1; i++)
    {
      if (a[i] > a[i - 1] && a[i] > a[i + 1])
      {
        cnt++;
      }
    }
    cout << cnt << el;
  }
}

int main()
{
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);

  int t = 1;
  // cin >> t;
  // cin.ignore();
  while (t--)
  {
    solve();
  }
  return 0;
}

/*
3
0 1000 0
5
0 1 2 0 1
3
0 1 2
7
0 1 0 1 8 7 6
11
0 4 3 7 6 10 7 8 4 6 10
0
*/