#include <bits/stdc++.h>

#define ll long long
#define pb push_back
#define pii pair<int, int>
#define pll pair<ll, ll>
#define el endl
#define mi map<int, int>
#define mll map<ll, ll>
#define umi unordered_map<int, int>
#define umll unordered_map<ll, ll>
#define vi vector<int>
#define vll vector<ll>
#define all(vect) vect.begin(), vect.end()
#define reset(A) memset(A, 0, sizeof(A))
#define approx(n) fixed << setprecision(n)

using namespace std;

const int mod = 1e9 + 7;

void solve()
{
  int n, p;
  cin >> n >> p;
  int a[n];
  vi res;
  for (int i = 0; i < n; i++)
  {
    cin >> a[i];
  }
  res.pb(p);
  bool ok = true;
  for(int i = 1; i < n; i++)
  {
    if(a[i] < a[i - 1])
    {
      p--;
    }
    if(a[i] == a[i - 1])
    {
      ok = false;
    }
    res.pb(p);
  }
  if(ok || p != 0) {

  }
  for (int i = 0; i < n; i++)
  {
    cout << res[i] << " ";
  }
}

int main()
{
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);

  int t = 1;
  // cin >> t;
  // cin.ignore();
  while (t--)
  {
    solve();
  }
  return 0;
}

/*
9 3
140
75
101
120
30
70
200
0
0
*/