#include <bits/stdc++.h>

#define ll long long
#define pb push_back
#define pii pair<int, int>
#define pll pair<ll, ll>
#define el endl
#define umi unordered_map<int, int>
#define umll unordered_map<ll, ll>
#define vi vector<int>
#define vll vector<ll>
#define all(vect) vect.begin(), vect.end()
#define reset(A) memset(A, 0, sizeof(A))
#define approx(n) fixed << setprecision(n)

using namespace std;

const int mod = 1e9 + 7;

void solve()
{
    ll n;
    cin >> n;
    int a = 0, b = 0, c = 0;
    while (n)
    {
        int tmp = n % 10;
        if (tmp < 5)
        {
            a++;
        }
        else if (tmp == 5)
        {
            b++;
        }
        else
        {
            c++;
        }
        n /= 10;
    }
    cout << a << " " << b << " " << c << el;
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int t = 1;
    cin >> t;
    // cin.ignore();
    while (t--)
    {
        solve();
    }
    return 0;
}

/*

*/