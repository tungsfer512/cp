#include <bits/stdc++.h>

#define ll long long
#define pb push_back
#define pii pair<int, int>
#define pll pair<ll, ll>
#define el endl
#define umi unordered_map<int, int>
#define umll unordered_map<ll, ll>
#define vi vector<int>
#define vll vector<ll>
#define all(vect) vect.begin(), vect.end()
#define reset(A) memset(A, 0, sizeof(A))
#define approx(n) fixed << setprecision(n)

using namespace std;

const int mod = 1e9 + 7;

void solve()
{
  int n;
  cin >> n;
  int a[n];
  for (int i = 0; i < n; i++)
  {
    cin >> a[i];
  }
  sort(a, a + n);
  int t1 = a[0] * a[1] * a[2];
  int t4 = a[n - 1] * a[n - 2] * a[n - 3];
  int t2 = a[0] * a[1] * a[n - 1];
  int t3 = a[0] * a[n - 1] * a[n - 2];

  cout << max(t1, max(t2, max(t3, t4))) << el;
}

int main()
{
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);

  int t = 1;
  // cin >> t;
  // cin.ignore();
  while (t--)
  {
    solve();
  }
  return 0;
}

/*
5
10 -10 20 -2 3
*/